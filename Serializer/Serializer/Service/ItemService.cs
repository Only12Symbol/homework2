﻿using System;
using System.Collections.Generic;
using System.Linq;
using Serializer.Poco;
using Serializer.Repository;

namespace Serializer.Service
{
    public class ItemService
    {
        private readonly IItemRepository _itemRepository;
        private const int MaxBulk = 100; 

        // Realization for IoC.
        public ItemService(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public bool AddItem(Item item)
        {
            var currentBulk = _itemRepository.GetItemsBulk();

            if (currentBulk + item.Bulk > MaxBulk)
            {
                return false;
            }

            _itemRepository.Add(item);
            return true;
        }

        public bool AddRange(IEnumerable<Item> items)
        {
            var itemsBulk = items.Sum(x => x.Bulk);
            var currentBulk = _itemRepository.GetItemsBulk();

            if (currentBulk + itemsBulk > MaxBulk)
            {
                return false;
            }

            _itemRepository.AddRange(items);
            return true;
        }
    }
}
