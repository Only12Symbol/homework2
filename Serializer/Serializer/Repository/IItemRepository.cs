﻿using System;
using Serializer.Poco;

namespace Serializer.Repository
{
    public interface IItemRepository : IRepository<Item>
    {
        public int GetItemsBulk();
    }
}
