﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Serializer.Repository
{
    public interface IRepository<T>
    {
        public void Add(T item);

        public void AddRange(IEnumerable<T> items);
        public T GetOne(Expression<Func<T, bool>> predicate);
        public IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate = null);
        public void Update(T item);
        public bool Delete(T item);
    }
}
