﻿using System.Linq;
using Serializer.Poco;

namespace Serializer.Repository.Implementation
{
    public class ItemRepository : RepositoryBase<Item>, IItemRepository
    {
        public int GetItemsBulk()
        {
            var items = GetAll();
            return items.Sum(item => item.Bulk);
        }

        public override void Update(Item item)
        {
            var currentItem = GetOne(x => x.Name == item.Name);
            Delete(currentItem);
            Add(item);
        }
    }
}