﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Serializer.Repository.Implementation
{
    public class RepositoryBase<T> : IRepository<T>
    {
        private static readonly List<T> Items = new List<T>();
        
        public void Add(T item)
        {
            Items.Add(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            Items.AddRange(items);
        }

        public T GetOne(Expression<Func<T, bool>> predicate)
        {
            return Items.Single(predicate.Compile());
        }

        public IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate = null)
        {
            var items = 
                predicate == null 
                    ? Items.ToList() 
                    : Items.Where(predicate.Compile());

            foreach (var item in items)
            {
                yield return item;
            }
        }

        public virtual void Update(T item)
        {
            // Can't update without id. Need dbset, or smth similar.
            throw new NotImplementedException();
        }

        public bool Delete(T item)
        {
            return Items.Remove(item);
        }
    }
}
