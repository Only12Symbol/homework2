﻿using System.IO;

namespace Serializer.Serializer
{
    public interface ISerializer
    {
        public string Serialize<T>(T item);
        public T Deserialize<T>(string data);
    }
}