﻿using System;
using SystemSerializer = System.Text.Json.JsonSerializer;

namespace Serializer.Serializer.Implementation
{
    public class JsonSerializer : ISerializer
    {
        public string Serialize<T>(T item)
        {
            return SystemSerializer.Serialize(item);
        }

        public T Deserialize<T>(string data)
        {
            return SystemSerializer.Deserialize<T>(data);
        }
    }
}
