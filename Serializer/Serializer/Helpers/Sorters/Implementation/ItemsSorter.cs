﻿using System;
using System.Collections.Generic;
using System.Linq;
using Serializer.Poco;

namespace Serializer.Helpers.Sorters.Implementation
{
    public class ItemsSorter : ISorter<Item>
    {
        /// <summary>
        /// Sort by name.
        /// </summary>
        /// <param name="items">Items.</param>
        /// <returns>Sorted items.</returns>
        public IEnumerable<Item> Sort(IEnumerable<Item> items)
        {
            return items.OrderBy(x => x.Name);
        }
    }
}
