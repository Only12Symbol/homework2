﻿using System.Collections.Generic;

namespace Serializer.Helpers.Sorters
{
    public interface ISorter<T>
    {
        public IEnumerable<T> Sort(IEnumerable<T> items);
    }
}