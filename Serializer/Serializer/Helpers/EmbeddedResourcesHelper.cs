﻿using System.IO;
using System.Reflection;

namespace Serializer.Helpers
{
    public class EmbeddedResourcesHelper
    {
        public static string GetData(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using var stream = assembly.GetManifestResourceStream(resourceName);
            using var reader = new StreamReader(stream!);

            return reader.ReadToEnd();
        }
    }
}