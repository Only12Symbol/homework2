﻿#nullable enable
namespace Serializer.Poco
{
    public class Item
    {
        public string Name { get; set; } = null!;
        public int Bulk { get; set; }
        public string Description { get; set; } = null!;

        public override string ToString()
        {
            return $"Name = {Name}, Bulk = {Bulk}, Description = {Description}";
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() 
                   ^ Bulk.GetHashCode() 
                   ^ Description.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            var item = (Item) obj;

            if (item == null)
            {
                return false;
            }

            return item.Bulk == Bulk 
                   && item.Name == Name 
                   && item.Description == Description;
        }
    }
}