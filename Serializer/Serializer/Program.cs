﻿using System;
using Serializer.Helpers;
using Serializer.Helpers.Sorters.Implementation;
using Serializer.Poco;
using Serializer.Serializer.Implementation;

namespace Serializer
{
    public class Program
    {
        public static void Main()
        {
            var data = EmbeddedResourcesHelper.GetData("Serializer.TestData.json");
            var serializer = new JsonSerializer();
            var reader = new ReaderEnumerable<Item>(data, serializer);
            var sorted = new ItemsSorter().Sort(reader);

            foreach (var item in sorted)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}