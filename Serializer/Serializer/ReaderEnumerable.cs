﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Serializer.Serializer;

namespace Serializer
{
    public class ReaderEnumerable<T> : IEnumerable<T>, IDisposable
    {
        private readonly IEnumerable<T> _items;

        public ReaderEnumerable(string data, ISerializer serializer)
        {
            _items = serializer.Deserialize<T[]>(data);
            
            if (_items == null)
            {
                _items = Enumerable.Empty<T>();
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in _items)
            {
                yield return item;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Nothing to dispose.
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        ~ReaderEnumerable()
        {
            Dispose(false);
        }

    }
}