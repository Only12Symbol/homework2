﻿using AutoFixture;
using Moq;
using Serializer.Poco;
using Serializer.Repository;
using Serializer.Service;
using Xunit;

namespace UnitTests.Serializer.Service
{
    public class ItemServiceTests
    {
        private readonly ItemService _itemService;
        private readonly Mock<IItemRepository> _mockItemRepository;

        public ItemServiceTests()
        {
            _mockItemRepository = new Mock<IItemRepository>();
            _itemService = new ItemService(_mockItemRepository.Object);
        }

        [Fact]
        public void AddItem_WhenBulkSumLesserThenMaxBulk_AddedSuccessfully()
        {
            var item = new Fixture().Build<Item>()
                .With(x => x.Bulk, 100)
                .Create();

            _itemService.AddItem(item);
            _mockItemRepository.Verify(x => x.Add(item), Times.Once);
        }

        [Fact]
        public void AddItem_WhenBulkSumMuchThenMaxBulk_ItemNotAdded()
        {
            var item = new Fixture().Build<Item>()
                .With(x => x.Bulk, int.MaxValue)
                .Create();

            _itemService.AddItem(item);
            _mockItemRepository.Verify(x => x.Add(item), Times.Never);
        }
    }
}